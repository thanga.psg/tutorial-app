import VueRouter from 'vue-router';
import AddTutorial from '../components/AddTutorial.vue';
import Tutorials from '../components/Tutorials.vue';
import Error404 from '../components/404.vue';

// Route path data
let routes = [
    {
      path: '/',
      component:  Tutorials,     // listing the tutorials when no path given
    },
    {
      path: '/tutorials',        // listing the tutorials
      component:  Tutorials,
    },
    {
      path: '/tutorial/:id',     // Editing the tutorial using addTutorial component
      component:  AddTutorial,
      props: { isEdit: true }    // with some props
    },
    {
      path: '/add',
      component:  AddTutorial,
    },
    {
      path: '/:catchAll(.*)*',   // it will display 404 for other paths
      component:  Error404,
    },
];

// Adding routes to VueRouter
export default new VueRouter({
    routes
});