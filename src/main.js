import Vue from 'vue'
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'
import App from './App.vue'
import routes from './router/routes.js'
import myMixin from './mixins/mixins';

Vue.config.productionTip = false

Vue.use(ElementUI, { locale });

Vue.use(VueRouter)

Vue.mixin(myMixin)

new Vue({
  render: h => h(App),
  router: routes,
}).$mount('#app')
