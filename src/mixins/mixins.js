let myMixin = {
  methods: {
    // showing message 
    showMsg: function (type, message) {
      this.$message({
        type,
        message
      })
    },
    // showing notification with title 
    showNoty: function (title, type, message) {
      this.$notify({
        title,
        type,
        message
      })
    },
  }
}

// mixins 
export default myMixin;