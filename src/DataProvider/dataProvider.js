
import data from './data.js'

/*

// In this app i used LocalStorage as data server
// otherwise we can use axios to get the data from API server

import axios from 'axios'

let api = axios.create({
  baseURL: "http://localhost:8080/api",
  headers: {
    "Content-type": "application/json"
  }
})

export function getAllTutorials() {
  return api.get('tutorials');
} 

*/

// setting the static data to localStorage if its empty
if(!localStorage.getItem('tutorialData')){
  setData(data)
}

// getting data from localStorage and using as data result
let dataAPI = JSON.parse(localStorage.getItem('tutorialData'))

function setData (data){
  localStorage.setItem('tutorialData', JSON.stringify(data))
}

// getting all tutorials 
export function getAllTutorials() {
  return dataAPI;
}

// getting tutorial using id
export function getTutorial(id) {
  return dataAPI.find(i=> i.id == id)
}

// creating tutorial 
export function addTutorial(payload) {
  // to get last id of array
  payload.id = dataAPI.length > 0  ? dataAPI.reduce((element,max) => element.id > max ? element.id : max, 0)?.id + 1  : 1
  dataAPI.push(payload)
  setData(dataAPI)
  return true
}

// deleting tutorial using id
export function deleteTutorial(id) {
  dataAPI.splice(dataAPI.findIndex(i=> i.id == id), 1)
  setData(dataAPI)
  return true
}

// updating tutorial 
export function updateTutorial(payload) {
  dataAPI.splice(dataAPI.findIndex(i=> i.id == payload.id), 1, payload)
  setData(dataAPI)
  return true
}

// deleting all tutorials 
export function deleteAllTutorials() {
  dataAPI = []
  setData(dataAPI)
  return true
}
