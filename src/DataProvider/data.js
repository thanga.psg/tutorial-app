
//sample data 

let data = [{
  id: 1,
  title: 'Vue.js',
  description: 'No. 189, Grove St, Los Angeles',
  status: 'pending'
  }, {
  id: 2,
  title: 'Javascript',
  description: 'No. 189, Grove St, Los Angeles',
  status: 'published'
  }, {
  id: 3,
  title: 'Java',
  description: 'No. 189, Grove St, Los Angeles',
  status: 'published'
  }, {
  id: 4,
  title: 'PHP',
  description: 'No. 189, Grove St, Los Angeles',
  status: 'pending'
}]

export default data;